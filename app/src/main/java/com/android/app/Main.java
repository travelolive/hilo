package com.android.app;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Main extends Activity {
    Toast toast;
    long backpressedtime = 100;
    MediaPlayer mp;
    boolean isForeground;
    SharedPreferences sp;
    SoundPool sndpool;
    int sndWin, moveToPoint, resultSteps;
    List<PointF> points = new ArrayList<>();
    float step;
    Bitmap chartBitmap;
    View circleActive, circleSaved;
    ImageView chart;
    AnimatorSet animChart;
    int action = 0; // 0 - nothing, 1 - up pressed, 2 - down pressed

    // config
    final String LINES_COLOR = "#FFFFFF"; // lines color
    final float CHART_HEIGHT = 0.3f; // chart height (max = 1)
    final int LINES_WIDTH = 2; // lines width
    final int NUM_POINTS = 10; // number of points on the screen
    final int ANIM_SPEED = 500; // chart moving speed
    final int RESULT_STEPS = 1; // number of steps to show result (can be >= 1)
    final int DELTA = 20; // wave force
    final int PERCENT = 10; // profit percent
    final int BET = 1; // bet
    int total = 1000; // cash on start

    // AdMob
    AdView adMobBanner;
    InterstitialAd adMobInterstitial;
    AdRequest adRequest;

    final int interstitialAdInterval = 5; // show InterstitialAd after each 5 win

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // sp
        sp = PreferenceManager.getDefaultSharedPreferences(this);

        // fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // AdMob
        adMob();

        // bg sound
        mp = new MediaPlayer();
        try {
            AssetFileDescriptor descriptor = getAssets().openFd("sndBg.mp3");
            mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.setLooping(true);
            if (isForeground)
                mp.setVolume(0.2f, 0.2f);
            else
                mp.setVolume(0, 0);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
        }

        // sound pool
        sndpool = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
        try {
            sndWin = sndpool.load(getAssets().openFd("sndWin.mp3"), 1);
        } catch (IOException e) {
        }

        // custom font
        Typeface font = Typeface.createFromAsset(getAssets(), "font.ttf");
        ((TextView) findViewById(R.id.txtTotal)).setTypeface(font);
        ((TextView) findViewById(R.id.txtBet)).setTypeface(font);
        ((TextView) findViewById(R.id.txtPercent)).setTypeface(font);
        ((TextView) findViewById(R.id.txtProfit)).setTypeface(font);
        ((TextView) findViewById(R.id.txtTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.txtMess)).setTypeface(font);
        ((TextView) findViewById(R.id.txtUp)).setTypeface(font);
        ((TextView) findViewById(R.id.txtDown)).setTypeface(font);

        ((TextView) findViewById(R.id.txtTotal)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(14));
        ((TextView) findViewById(R.id.txtBet)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(14));
        ((TextView) findViewById(R.id.txtPercent)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(14));
        ((TextView) findViewById(R.id.txtProfit)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(14));

        ((TextView) findViewById(R.id.txtTitle)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(40));
        ((TextView) findViewById(R.id.txtMess)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(30));

        ((TextView) findViewById(R.id.txtUp)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(20));
        ((TextView) findViewById(R.id.txtDown)).setTextSize(TypedValue.COMPLEX_UNIT_PX, DpToPx(20));

        // elements
        chart = (ImageView) findViewById(R.id.chart);
        circleActive = findViewById(R.id.circleActive);
        circleSaved = findViewById(R.id.circleSaved);
        circleActive.setVisibility(View.INVISIBLE);
        circleSaved.setAlpha(0f);

        // circle size
        circleActive.getLayoutParams().width = circleActive.getLayoutParams().height = circleSaved.getLayoutParams().width = circleSaved.getLayoutParams().height = Math.round(DpToPx(LINES_WIDTH) * 4f);

        updateText();

        // on start
        findViewById(R.id.game).post(new Runnable() {
            @Override
            public void run() {
                chart.getLayoutParams().height = Math.round((float) findViewById(R.id.game).getHeight() * CHART_HEIGHT);
                findViewById(R.id.imgArrows).getLayoutParams().width = findViewById(R.id.game).getWidth() / 4; // arrows size
                findViewById(R.id.chartBack).getLayoutParams().width = findViewById(R.id.chartOver).getLayoutParams().width = findViewById(R.id.game).getWidth() / 2; // chart back and over size
                findViewById(R.id.imgArrows).setVisibility(View.GONE);
                findViewById(R.id.imgArrows).setVisibility(View.INVISIBLE);

                chart.post(new Runnable() {
                    @Override
                    public void run() {
                        step = (float) findViewById(R.id.game).getWidth() / (float) NUM_POINTS * 0.5f;
                        updatePoints();
                        animate();
                    }
                });
            }
        });

        // hide navigation bar listener
        findViewById(R.id.root).setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                hideNavigation();
            }
        });
    }

    // onClick
    public void onClick(View v) {
        if (total >= BET) {
            resultSteps = 0;
            action = v.getId() == R.id.btnUp ? 1 : 2; // up/down
            total -= BET; // show total
            updateText();
            circleSaved.setBackgroundResource(v.getId() == R.id.btnUp ? R.drawable.circle_saved_up : R.drawable.circle_saved_down);

            // hide buttons
            findViewById(R.id.btnUp).setEnabled(false);
            findViewById(R.id.btnDown).setEnabled(false);
            findViewById(R.id.buttons).setVisibility(View.GONE);
        } else TOAST(R.string.error_cash);
    }

    @Override
    public void onBackPressed() {
        if (!sp.contains("rate")) rateDialog();
        else if (System.currentTimeMillis() - backpressedtime > 1000) {
            backpressedtime = System.currentTimeMillis();
            TOAST(R.string.exit);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        // clear chart bitmap
        if (chartBitmap != null)
            chartBitmap.recycle();
        chartBitmap = null;

        // stop animChart
        if (animChart != null) {
            animChart.removeAllListeners();
            animChart.cancel();
        }

        // toast
        if (toast != null)
            toast.cancel();

        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            hideNavigation();
    }

    // updateText
    void updateText() {
        ((TextView) findViewById(R.id.txtTotal)).setText(getString(R.string.txt_total) + total);
        ((TextView) findViewById(R.id.txtBet)).setText(getString(R.string.txt_bet) + BET);
//        ((TextView) findViewById(R.id.txtPercent)).setText(getString(R.string.txt_percent) + PERCENT + getString(R.string.percent));
        ((TextView) findViewById(R.id.txtPercent)).setText(getString(R.string.txt_percent) + 95 + getString(R.string.percent));
//        ((TextView) findViewById(R.id.txtProfit)).setText(getString(R.string.txt_profit) + Math.round(BET + (float) BET * (float) PERCENT / 100f));
        ((TextView) findViewById(R.id.txtProfit)).setText(getString(R.string.txt_profit) +  0.10f);
    }

    // updatePoints
    void updatePoints() {
        // minY and maxY
        int minY = circleActive.getHeight();
        int maxY = chart.getHeight() - circleActive.getHeight();

        // add points
        PointF newPoint;
        for (int i = points.size(); i <= NUM_POINTS * 2; i++) {
            newPoint = i == 0 ? new PointF(0, minY + (maxY - minY) * 0.5f) : new PointF(points.get(points.size() - 1).x, points.get(points.size() - 1).y);
            newPoint.x = i * step;
            newPoint.y += (Math.round(Math.random() * DpToPx(DELTA * 2) - DpToPx(DELTA))); // random Y

            // check limits
            if (newPoint.y < minY)
                newPoint.y = minY;
            if (newPoint.y > maxY)
                newPoint.y = maxY;

            points.add(new PointF(newPoint.x, newPoint.y));
        }

        moveToPoint = NUM_POINTS + 1;
        drawChart(); // draw chart

        // circle active position
        circleActive.setVisibility(View.VISIBLE);
        circleActive.setX(points.get(moveToPoint - 1).x - circleActive.getWidth() * 0.5f);
        circleActive.setY(points.get(moveToPoint - 1).y - circleActive.getHeight() * 0.5f + chart.getY());

        // circle saved position
        if (findViewById(R.id.buttons).getVisibility() == View.GONE) {
            int deltaPoint = Math.max(RESULT_STEPS, 1) - resultSteps;
            circleSaved.setX(points.get(moveToPoint - deltaPoint - 2).x - circleSaved.getWidth() * 0.5f);
            circleSaved.setY(points.get(moveToPoint - deltaPoint - 2).y - circleSaved.getHeight() * 0.5f + chart.getY());
        }
    }

    // drawChart
    void drawChart() {
        // clear chart bitmap
        if (chartBitmap != null)
            chartBitmap.recycle();
        chartBitmap = null;

        // create chart bitmap
        chartBitmap = Bitmap.createBitmap(chart.getWidth(), chart.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(chartBitmap);

        // paint style
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor(LINES_COLOR));
        paint.setStrokeWidth(DpToPx(LINES_WIDTH));
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setPathEffect(new CornerPathEffect(DpToPx(LINES_WIDTH)));
        //paint.setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));

        // draw lines
        Path path = new Path();
        for (int i = 0; i < points.size(); i++)
            if (i == 0)
                path.moveTo(points.get(i).x, points.get(i).y);
            else
                path.lineTo(points.get(i).x, points.get(i).y);
        canvas.drawPath(path, paint);

        // set chart bitmap
        chart.setImageBitmap(chartBitmap);
    }

    // animate
    void animate() {
        List<Animator> animList = new ArrayList<>();

        // stop action
        if (resultSteps == -2) {
            findViewById(R.id.txtMess).setVisibility(View.GONE);

            // show buttons
            findViewById(R.id.btnUp).setEnabled(true);
            findViewById(R.id.btnDown).setEnabled(true);
            findViewById(R.id.buttons).setVisibility(View.VISIBLE);
        } else
            resultSteps--;

        // action
        if (action != 0) {
            if (findViewById(R.id.txtMess).getVisibility() == View.GONE) {
                // first anim after save point
                resultSteps = Math.max(RESULT_STEPS, 1);
                ((TextView) findViewById(R.id.txtMess)).setText(String.valueOf(resultSteps));
                findViewById(R.id.txtMess).setVisibility(View.VISIBLE);

                // show saved circle
                circleSaved.setX(circleActive.getX());
                circleSaved.setY(circleActive.getY());
                animList.add(ObjectAnimator.ofFloat(circleSaved, "alpha", 1f));
            } else {
                switch (resultSteps) {
                    case 0: // check result
                        checkResult();
                        break;
                    case -1: // stop action
                        action = 0;
                        animList.add(ObjectAnimator.ofFloat(circleSaved, "alpha", 0f));
                        break;
                    default: // show seconds
                        ((TextView) findViewById(R.id.txtMess)).setText(String.valueOf(resultSteps));
                        break;
                }
            }

            // animate mess
            if (resultSteps != -1) {
                findViewById(R.id.txtMess).setAlpha(0f);
                animList.add(ObjectAnimator.ofFloat(findViewById(R.id.txtMess), "alpha", 1f));
            }
        }

        animList.add(ObjectAnimator.ofFloat(chart, "x", chart.getX() - step));
        animList.add(ObjectAnimator.ofFloat(circleActive, "y", points.get(moveToPoint).y - circleActive.getHeight() * 0.5f + chart.getY()));
        animList.add(ObjectAnimator.ofFloat(circleSaved, "x", circleSaved.getX() - step));

        // animate
        animChart = new AnimatorSet();
        animChart.setDuration(ANIM_SPEED);
        animChart.setStartDelay(ANIM_SPEED);
        animChart.playTogether(animList);
        animChart.setInterpolator(new LinearInterpolator());
        animChart.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // last point
                if (moveToPoint == points.size() - 1) {
                    // split points
                    points = points.subList(points.size() - 1 - NUM_POINTS, points.size());

                    // update points X
                    circleSaved.setX(circleActive.getX() + chart.getX());
                    for (int i = 0; i < points.size(); i++)
                        points.get(i).x += chart.getX();

                    chart.setX(0); // move chart on start
                    updatePoints(); // update chart
                } else
                    moveToPoint++;

                animate();
            }

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animChart.start();
    }

    // checkResult
    void checkResult() {
        if ((action == 1 && circleActive.getY() < circleSaved.getY()) || (action == 2 && circleActive.getY() > circleSaved.getY())) {
            ((TextView) findViewById(R.id.txtMess)).setText(getString(R.string.mess_win) + Math.round(BET + (float) BET * (float) PERCENT / 100f));

            // total
            total += Math.round(BET + (float) BET * (float) PERCENT / 100f);
            updateText();

            // sound
            if (isForeground)
                sndpool.play(sndWin, 0.9f, 0.9f, 0, 0, 1);

            // AdMob Interstitial
            if (adMobInterstitial != null) {
                sp.edit().putInt("admob", !sp.contains("admob") ? 1 : sp.getInt("admob", 0) + 1).commit();
                if (adMobInterstitial.isLoaded()) {
                    if (sp.getInt("admob", 0) >= interstitialAdInterval) {
                        sp.edit().putInt("admob", 0).commit();
                        adMobInterstitial.show(); // show
                    }
                } else if (!adMobInterstitial.isLoading())
                    adMobInterstitial.loadAd(adRequest); // load
            }
        }
        else
            ((TextView) findViewById(R.id.txtMess)).setText(getString(R.string.mess_lose));
    }

    @Override
    protected void onPause() {
        isForeground = false;
        if (mp != null)
            mp.setVolume(0, 0);

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isForeground = true;

        if (mp != null)
            mp.setVolume(0.2f, 0.2f);
    }

    // hideNavigation
    void hideNavigation() {
        // fullscreen mode
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    // TOAST
    void TOAST(int mess) {
        if (toast != null)
            toast.cancel();
        toast = Toast.makeText(this, getString(mess), Toast.LENGTH_SHORT);
        ((TextView) ((LinearLayout) toast.getView()).getChildAt(0)).setGravity(Gravity.CENTER);
        toast.show();
    }

    // DpToPx
    float DpToPx(float dp) {
        return (dp * Math.max(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels) / 540f);
    }

    // rateDialog
    void rateDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setMessage(R.string.rate_ask);
        adb.setPositiveButton(getString(R.string.rate_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                sp.edit().putBoolean("rate", true).commit();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                finish();
            }
        });
        adb.setNegativeButton(getString(R.string.rate_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog dialog = adb.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ((TextView) dialog.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
    }

    // adMob
    void adMob() {
        if (getResources().getBoolean(R.bool.show_admob)) {
            MobileAds.initialize(this);

            // make AdMob request
            AdRequest.Builder builder = new AdRequest.Builder();
            if (getResources().getBoolean(R.bool.admob_test))
                builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice(
                        MD5(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
            adRequest = builder.build();

            // AdMob Interstitial
            adMobInterstitial = new InterstitialAd(Main.this);
            adMobInterstitial.setAdUnitId(getString(R.string.adMob_interstitial));
            adMobInterstitial.setAdListener(new AdListener() {
                public void onAdClosed() {
                    adMobInterstitial.loadAd(adRequest);
                }
            });

            // AdMob Banner
            adMobBanner = new AdView(Main.this);
            adMobBanner.setAdUnitId(getString(R.string.adMob_banner));
            adMobBanner.setAdSize(AdSize.SMART_BANNER);
            ((ViewGroup) findViewById(R.id.admob)).addView(adMobBanner);

            // load
            adMobBanner.loadAd(adRequest);
            adMobInterstitial.loadAd(adRequest);
        }
    }

    // MD5
    String MD5(String str) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(str.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i)
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            return sb.toString().toUpperCase(Locale.ENGLISH);
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}